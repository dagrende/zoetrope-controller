#include <Arduino.h>

#define MOTOR_PIN 9
// #define FLASH LED_BUILTIN_RX
// #define FLASH_ON 0
#define FLASH_PIN 2
#define FLASH_ON -1

// look at https://www.luisllamas.es/en/arduino-pidcontroller-library/
// #define P 1
// #define I 1
// #define D 1
// int force = 0;
unsigned long now, time, lastTabTime, nextPrintTime;

void setup() {
  pinMode(FLASH_PIN, OUTPUT);
  pinMode(15, INPUT_PULLUP);
  digitalWrite(FLASH_PIN, ~FLASH_ON);
  pinMode(MOTOR_PIN, OUTPUT);
  analogWrite(MOTOR_PIN, 127);
  Serial.begin(9600);
}

int lastIn = 1;
void loop() {
  int in = digitalRead(15);
  if (lastIn < in) {
    digitalWrite(FLASH_PIN, FLASH_ON);
    delayMicroseconds(500);
    digitalWrite(FLASH_PIN, ~FLASH_ON);
    now = millis();
    time = now - lastTabTime;
    lastTabTime = now;
  }
  lastIn = in;
  if (nextPrintTime < now) {
    nextPrintTime = now + 1000;
    Serial.println(time);
  }
}
